# Shoyu Ramen

![](https://pbs.twimg.com/media/CakubVOWwAAUuYO.jpg)

## The Broth

* 1/4 C. Shoyu soy sauce
* 32 oz. chicken stock
* Half a bigass leek
* About a thumb’s worth of ginger, slivered
* Four cloves of garlic, minced
* ~2 Tbsp. carrot, minced
* A couple shortribs; I used maybe half a pound? I don’t know; I’m a vegetarian, for Chrissakes. Why are you listening to me?
* ~6"x2" sheet of kombu
* 1/8 C. dried woodear mushrooms (at most)
* A little olive oil
* A little vegetable oil

Put a little olive oil in a stockpot, throw the garlic and carrots in there once the oil starts shimmering; if you see a plume of smoke, kill the heat for a second and cool it off. Don't brown your garlic, holmes. Cook this until you feel like you are done cooking this; prolly just a couple minutes. Add the ginger, stock, soy sauce, and leek half (probably in two big chunks) and bring it all to a boil. Meantime, throw a _little_ vegetable oil into a skillet—cast iron if you got it—turn the heat up higher than feels responsible, and juuuust brown the shortribs. Once browned on all sides, pour the whole thing—oil and all—into the now-boiling stockpot. Reduce heat and simmer for an hour and a half.

Set the shortribs aside and strain the solids out of your broth, then return the broth to your pot. Add the kombu and the mushrooms, if you’re into mushrooms; I am not, because I have good taste. Simmer for another half hour or so, but more would be better. Let the broth cool and separate some, then skim off an amount of the fat; I dunno, use your best judgment. You can then chuck this in the fridge overnight, if you really want to commit to this thing.

## The Whole Deal

* A mess of ramen-style noodles, like _chuka soba_
* A fistful of baby spinach
* Scallions
* A couple soft-boiled eggs
* Whatever else you want in there

Crank your broiler up. Throw the broth back on the heat. Start boiling a small pot of water. Have a bowl of ice water at the ready.

Soft-boil the egg; six minutes to the second for me, but your milage may vary. Into the ice water it goes, to stop it from cooking further. Throw your fistful of spinach into that very same water for about a minute, then put all that in the ice water too. Leave everything in there for a minute (so two minutes total, for the egg), then remove it all and set it aside on a paper towel.

Take the shortribs, brush them with a little more of the soy sauce, and broil them up until they get a nice dark crisp goin’—you can sear them in a skillet real quick, too, just make sure they’re nice and dry first.

Okay. Now assemble the whole deal: cooked noodles in the bowl (slightly undercooked, since the hot broth is gonna get them the rest of the way there), broth over the noodles (remove the big sheet of kombu, first), fistful of blanched spinach, chuck a rib or two and half an egg in there, garnish with scallions and a little kombu chiffonade, if that’s your thing. You can’t really go wrong beyond this point; a scoop of corn adds sweetness, some julienned bamboo shoots or beansprouts will add a little texture.